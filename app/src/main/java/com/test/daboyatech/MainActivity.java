package com.test.daboyatech;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.test.daboyatech.adapter.ImageAdapter;
import com.test.daboyatech.models.ImageDataModel;
import com.test.daboyatech.viewmodel.ImageViewModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private String page = "1";
    private final String LIMIT = "100";

    private ImageAdapter adapter;
    private ImageViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new ImageAdapter();
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.setAdapter(adapter);

        viewModel = new ViewModelProvider(this).get(ImageViewModel.class);
        viewModel.init();
        viewModel.getImageResponseLiveData().observe(this, new Observer<ArrayList<ImageDataModel>>() {
            @Override
            public void onChanged(ArrayList<ImageDataModel> response) {
                if (response != null) {
                    adapter.setResults(response);
                }
            }
        });
        viewModel.getImages(page, LIMIT);
    }
}