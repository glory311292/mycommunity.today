package com.test.daboyatech.network;

import com.test.daboyatech.models.ImageDataModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ImageService {
    @GET("v2/list")
    Call<ArrayList<ImageDataModel>> getImageFromServer(
            @Query("page") String page,
            @Query("limit") String limit
    );
}
