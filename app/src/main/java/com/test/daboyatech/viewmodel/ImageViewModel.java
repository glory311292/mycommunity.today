package com.test.daboyatech.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.test.daboyatech.models.ImageDataModel;
import com.test.daboyatech.network.ImageRepository;

import java.util.ArrayList;


public class ImageViewModel extends AndroidViewModel {
    private ImageRepository imageRepository;
    private LiveData<ArrayList<ImageDataModel>> imageResponseLiveData;

    public ImageViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        imageRepository = new ImageRepository();
        imageResponseLiveData = imageRepository.getImageResponseLiveData();
    }

    public void getImages(String page, String limit) {
        imageRepository.getImageFromServer(page, limit);
    }

    public LiveData<ArrayList<ImageDataModel>> getImageResponseLiveData() {
        return imageResponseLiveData;
    }
}
