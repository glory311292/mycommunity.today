package com.test.daboyatech.network;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.test.daboyatech.models.ImageDataModel;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImageRepository {
    private static final String BOOK_SEARCH_SERVICE_BASE_URL = "https://picsum.photos/";

    private ImageService imageService;
    private MutableLiveData<ArrayList<ImageDataModel>> imageResponseLiveData;

    public ImageRepository() {
        imageResponseLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        imageService = new retrofit2.Retrofit.Builder()
                .baseUrl(BOOK_SEARCH_SERVICE_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ImageService.class);

    }

    public void getImageFromServer(String page, String limit) {
        imageService.getImageFromServer(page, limit)
                .enqueue(new Callback<ArrayList<ImageDataModel>>() {
                    @Override
                    public void onResponse(Call<ArrayList<ImageDataModel>> call, Response<ArrayList<ImageDataModel>> response) {
                        if (response.body() != null) {
                            imageResponseLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ArrayList<ImageDataModel>> call, Throwable t) {
                        imageResponseLiveData.postValue(null);
                    }
                });
    }

    public LiveData<ArrayList<ImageDataModel>> getImageResponseLiveData() {
        return imageResponseLiveData;
    }
}
